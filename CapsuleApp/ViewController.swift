//
//  ViewController.swift
//  CapsuleApp
//
//  Created by Keith on 27/08/2021.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var model: Model = {
        let model = Model()
        model.load()
        return model
    }()
    
    lazy var table: UITableView = {
        let table = UITableView()
        table.estimatedRowHeight = UITableView.automaticDimension
        table.rowHeight = UITableView.automaticDimension
        table.dataSource = self
        table.delegate = self
        table.translatesAutoresizingMaskIntoConstraints = false
        table.register(Cell.self, forCellReuseIdentifier: "cell")
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let guide = view.safeAreaLayoutGuide
        view.addSubview(table)
        table.leadingAnchor.constraint(equalTo: guide.leadingAnchor).isActive = true
        table.topAnchor.constraint(equalTo: guide.topAnchor).isActive = true
        table.trailingAnchor.constraint(equalTo: guide.trailingAnchor).isActive = true
        table.bottomAnchor.constraint(equalTo: guide.bottomAnchor).isActive = true
        

//        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTap)))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        table.reloadData()
    }

    @objc func onTap() {
        model.load()
        table.reloadData()
    }

}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        model.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let info = model.rowInfo(index: indexPath.row)
        guard let cell = table.dequeueReusableCell(withIdentifier: "cell") as? Cell else { return UITableViewCell() }
        cell.present(info, into: table.bounds.width)
        return cell
    }
}


class Cell: UITableViewCell {
    
    let imageWidth: CGFloat = 70
    let sideMargin: CGFloat = 20
    
    lazy var rowLabel: UILabel = {
        let label = UILabel()
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        label.backgroundColor = UIColor.systemBlue
        label.textColor = UIColor.white
        label.numberOfLines = 0
        return label
    }()
    
    lazy var image: UIView = {
       let view = UIView()
        view.widthAnchor.constraint(equalToConstant: imageWidth).isActive = true
        view.backgroundColor = UIColor.systemOrange
        return view
    }()
    
    lazy var mainStack: UIStackView = {
        let stack = UIStackView()
        stack.addArrangedSubview(image)
        stack.addArrangedSubview(verticalStack)
        stack.addArrangedSubview(UIView())
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    lazy var verticalStack: UIStackView = {
        let stack = UIStackView()
        stack.addArrangedSubview(rowLabel)
        stack.addArrangedSubview(capsules)
        stack.axis = .vertical
        stack.distribution = .fill
        return stack
    }()
    
    lazy var capsules: CapsuleCollectionView = {
        let capsules = CapsuleCollectionView()
        capsules.translatesAutoresizingMaskIntoConstraints = false
        capsules.backgroundColor = UIColor.systemPink
        return capsules
    }()
    
    func present(_ rowInfo: RowInfo, into width: CGFloat) {
        rowLabel.text = rowInfo.title
        capsules.reload(strings: rowInfo.words, width: width - imageWidth - 2 * sideMargin)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(mainStack)
        mainStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: sideMargin).isActive = true
        mainStack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: sideMargin).isActive = true
        mainStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: sideMargin).isActive = true
        mainStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -sideMargin).isActive = true
        mainStack.backgroundColor = UIColor.lightGray
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}

extension ViewController: UITableViewDelegate {
    
}


class Model {
    
    private var items = [RowInfo]()
    
    var numberOfRows: Int { items.count }
    func rowInfo(index: Int) -> RowInfo { items[index] }
    
    func load() {
        items = []
        (0...9).forEach { row in
            let number = (1...sourceWords.count).randomElement()
            let words = [String](self.sourceWords.prefix(number ?? 1))
            let title = "Row \(row) ---------------------------------"
            let info = RowInfo(title: title, words: words)
            items.append(info)
        }
    }
    
    private let sourceWords = [
        "aaa",
        "bbbb",
        "ccccc",
        "dddddd",
        "eeeeeee",
        "ffffffff"
    ]
}

struct RowInfo {
    var title: String
    var words: [String]
}
